def factors(number):
    # ==============
    # Your code here
    i = 2
    fact = []
    while i < number:
        if number % i == 0:
            fact.append(i)
        i= i+1
    if fact == []:
        prime = str(number) + " is a prime number"
        return prime
    else:
        return fact
    
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
